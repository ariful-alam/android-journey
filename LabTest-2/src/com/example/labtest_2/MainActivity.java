package com.example.labtest_2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	ImageView iv;
	AnimationDrawable ad;
	
	Button gobtn;
	MediaPlayer mp;
	boolean mp_state = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mp = MediaPlayer.create(this, R.raw.audio);
		mp.setLooping(true);
		
		if(!mp_state){
			mp.start();
			mp_state = true;
		}
		
		iv = (ImageView) findViewById(R.id.imageView1);
		iv.setImageResource(R.drawable.frameanimation);
		
		ad = (AnimationDrawable) iv.getDrawable();
		ad.start();
		
		gobtn = (Button) findViewById(R.id.go);
		gobtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(mp_state){
					mp.pause();
					mp_state = false;
					
					Intent intent = new Intent(MainActivity.this, Login_pre.class);
					startActivity(intent);
				}else{
					Toast.makeText(getApplicationContext(), "Already paused", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}
