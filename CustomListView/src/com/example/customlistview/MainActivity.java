package com.example.customlistview;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity{
	
	ListView list;
	String []country;
	List<String> countrylist;
	ArrayAdapter<String> aa;
	CustomAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		list = (ListView) findViewById(R.id.listView1);
		
		country = getResources().getStringArray(R.array.country);
		countrylist = Arrays.asList(country);
		adapter = new CustomAdapter(getApplicationContext(), countrylist); //ArrayAdapter<String>(where, data source)
		list.setAdapter(aa);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


}
