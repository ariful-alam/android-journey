package com.example.sharedpreference;

import java.util.Random;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	TextView tv1, tv2;
	Button btn;
	
	SharedPreferences sp;
	SharedPreferences.Editor spe;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tv1 = (TextView) findViewById(R.id.textView1);
		tv2 = (TextView) findViewById(R.id.textView2);
		
		btn = (Button) findViewById(R.id.button1);
		
		sp = getSharedPreferences("game", MODE_PRIVATE);
		spe = sp.edit();
		
		int hscore = sp.getInt("hscore", -1); //hscore = where to check score, default value
		tv1.setText("High Score: " + hscore);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Random r = new Random();
				int score = r.nextInt(1000);
				
				tv2.setText("Score: " + score);
				
				int getscore = sp.getInt("hscore", -1);
				
				if(score > getscore){
					tv1.setText("High Score: " + score);
					spe.putInt("hscore", score);
					spe.commit();
				}
			}
		});
	}
}
