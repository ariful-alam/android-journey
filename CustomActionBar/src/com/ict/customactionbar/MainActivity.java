package com.ict.customactionbar;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	
	ImageButton imgCall,imgMail,imgSend;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	    ViewGroup v = (ViewGroup)LayoutInflater.from(this).inflate(R.layout.action_bar_view, null);
		imgCall=(ImageButton)v.findViewById(R.id.imgCall);
		imgMail=(ImageButton)v.findViewById(R.id.imgMail);
		imgSend=(ImageButton)v.findViewById(R.id.imgSend);
		
		imgCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(),"Call is clicked" , Toast.LENGTH_LONG).show();
			}
		});
		
		imgMail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(),"Mail is clicked" , Toast.LENGTH_LONG).show();
			}
		});
		
		imgSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(),"Send is clicked" , Toast.LENGTH_LONG).show();
			}
		});
		
		actionBar=getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(v,new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
		
	}
	
	
	
	


}
