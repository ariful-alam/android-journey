package com.example.homework_2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

public class Page2 extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.page2);
        
        String value = getIntent().getExtras().getString("data");
        
        if(value.substring(0, 7).equalsIgnoreCase("http://")){//checks whitespace
        	Toast.makeText(getApplicationContext(), "Showing " + value + " in default browser", Toast.LENGTH_SHORT).show();
        	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(value));
    		startActivity(intent);
        }else{
        	Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT).show();
        }
	}
}
