package com.example.googlemap;

import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button show, distance, address, internet, provider;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		show = (Button) findViewById(R.id.btnShow);
		show.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, MapViewer.class);
				startActivity(intent);
			}
		});
		
		distance = (Button) findViewById(R.id.btnDistance);
		distance.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				double lat1 = 22.368025;
				double lon1 = 91.849106;
				double lat2 = 22.351451;
				double lon2 = 91.852068;
				float[] results = new float[1];
				
				Location.distanceBetween(lat1, lon1, lat2, lon2, results);
				double distance_km = results[0] / 1000;
				
				Toast.makeText(getApplicationContext(), "Distance: "+distance_km+" km", Toast.LENGTH_SHORT).show();
			}
		});
		
		address = (Button) findViewById(R.id.btnAddress);
		address.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Geocoder gcd = new Geocoder(MainActivity.this, Locale.getDefault());
				List<Address> address = null;
				Address addr = null;
				
				double lat = 22.368025;
				double lon = 91.849106;
				
				try{
					address = gcd.getFromLocation(lat, lon, 1);
					if(address.size() > 0){
						addr = address.get(0);
						String area = "Address is: ";
						if(addr.getMaxAddressLineIndex() > 0){
							area += addr.getAddressLine(0);
						}
						area += ", " + addr.getLocality() +", "+addr.getCountryName();
						Toast.makeText(getApplicationContext(), area, Toast.LENGTH_SHORT).show();
					}
				}catch(Exception e){
					Toast.makeText(getApplicationContext(), "Address not found", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		internet = (Button) findViewById(R.id.btnInternet);
		internet.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				boolean internetStatus = inConnectingInternet();
				
				if(internetStatus)
					Toast.makeText(getApplicationContext(), "Connection is ON", Toast.LENGTH_SHORT).show();
				else{
					Intent in = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			}
		});
		
		provider = (Button) findViewById(R.id.btnProvider);
		provider.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				LocationManager location = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				boolean gpsStatus = location.isProviderEnabled(LocationManager.GPS_PROVIDER);
				boolean netStatus = location.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
				
				if(gpsStatus)
					Toast.makeText(getApplicationContext(), "GPS Active", Toast.LENGTH_SHORT).show();
				if(netStatus)
					Toast.makeText(getApplicationContext(), "Network Active", Toast.LENGTH_SHORT).show();
				
				if(!gpsStatus || !netStatus){
					Intent in = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivity(in);
				}
			}
		});
	}
	
	public boolean inConnectingInternet(){
		ConnectivityManager con = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		
		if(con != null){
			NetworkInfo info = con.getActiveNetworkInfo();
			if(info != null && info.isConnected()) return true;
		}
		return false;
	}
}