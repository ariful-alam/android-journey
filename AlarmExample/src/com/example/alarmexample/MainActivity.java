package com.example.alarmexample;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends Activity {
	final String ACTION = "com.example.alarmexample.ALARM";
	
	TimePicker time;
	Button set_alarm, remove_alarm;
	
	AlarmManager manager;
	PendingIntent pending;
	int alarm_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		
		Intent intent = new Intent(ACTION);
		alarm_id = 0;
		pending = PendingIntent.getBroadcast(getApplicationContext(), alarm_id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		time = (TimePicker) findViewById(R.id.timePicker1);
		set_alarm = (Button) findViewById(R.id.button1);
		set_alarm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				int hour = time.getCurrentHour();
				int minute = time.getCurrentMinute();
				
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.HOUR_OF_DAY, hour);
				calendar.set(Calendar.MINUTE, minute);
				calendar.set(Calendar.SECOND, 0);
				
				manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pending);
				Toast.makeText(getApplicationContext(), "Alarm set done!", Toast.LENGTH_SHORT).show();
			}
		});
		
		remove_alarm = (Button) findViewById(R.id.button2);
		remove_alarm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				manager.cancel(pending);
			}
		});
	}
}
