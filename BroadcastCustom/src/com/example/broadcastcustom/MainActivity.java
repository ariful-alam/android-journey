package com.example.broadcastcustom;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	TextView txtv;
	EditText edit;
	Button btn_broadcast;
	
	broadcast_receiver br;
	IntentFilter intf;
	String ACTION = "com.example.broadcustevent"; //As wish :)
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		br = new broadcast_receiver();
		intf = new IntentFilter(ACTION);
		
		txtv = (TextView) findViewById(R.id.textView1);
		edit = (EditText) findViewById(R.id.editText1);
		
		btn_broadcast = (Button) findViewById(R.id.btn_broadcast);
		btn_broadcast.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String text = edit.getText().toString();
				if(text.equals("")){
					Toast.makeText(getApplicationContext(), "Input something", Toast.LENGTH_SHORT).show();
				}else{
					Intent intent = new Intent(ACTION);
					intent.putExtra("key", text);
					
					sendBroadcast(intent);
				}				
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		registerReceiver(br, intf);
		super.onResume();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(br);
		super.onDestroy();
	}
	
	class broadcast_receiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if(intent.getAction().equals(ACTION)){
				String _text = intent.getExtras().getString("key");
				txtv.setText(_text);
				Toast.makeText(getApplicationContext(), "Broadcast Received", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
