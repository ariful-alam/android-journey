package com.example.image4mserver;

import com.squareup.picasso.Picasso;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.view.Menu;
import android.widget.ImageView;

public class MainActivity extends Activity {
	ImageView image;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		image = (ImageView) findViewById(R.id.imageView1);
		ImageLoader imgload = new ImageLoader();
		imgload.execute();
	}
	
	class ImageLoader extends AsyncTask<String, String, String>{
		ProgressDialog dialog = new ProgressDialog(MainActivity.this);
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			//super.onPreExecute();
			dialog.setMessage("Image loading");
			dialog.setCancelable(false);
			dialog.show();
		}
		
		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			Picasso.with(MainActivity.this).load("http://upload.wikimedia.org/wikipedia/commons/5/59/Shakib-Al-Hasan-Face.jpg").into(image);
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			//super.onPostExecute(result);
			dialog.dismiss();
		}
		
	}
}
