package com.example.listviewexample;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	ListView lv;
	
	ArrayList<String> items = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		lv = (ListView) findViewById(R.id.listView1);
		
		items.add("Dhaka");
		items.add("Chittagong");
		items.add("Sylhet");
		items.add("Rajshahi");
		items.add("Borisal");
		items.add("Khulna");
		items.add("Rangpur");
		
		items.add("City 1");
		items.add("City 2");
		items.add("City 3");
		items.add("City 4");
		items.add("City 5");
		items.add("City 6");
		items.add("City 7");
		items.add("City 8");
		items.add("City 9");
		items.add("City 10");
		items.add("City 11");
		items.add("City 12");
		items.add("City 13");
		items.add("City 14");
		items.add("City 15");
		items.add("City 16");
		items.add("City 17");
		items.add("City 18");
		items.add("City 19");
		items.add("City 20");
		items.add("City 21");
		items.add("City 22");
		items.add("City 23");
		items.add("City 24");
		items.add("City 25");
		items.add("City 26");
		items.add("City 27");
		items.add("City 28");
		items.add("City 29");
		items.add("City 30");
		items.add("City 31");
		items.add("City 32");
		items.add("City 33");
		items.add("City 34");
		items.add("City 35");
		items.add("City 36");
		items.add("City 37");
		items.add("City 38");
		items.add("City 39");
		items.add("City 30");
		items.add("City 31");
		items.add("City 32");
		items.add("City 33");
		items.add("City 34");
		items.add("City 35");
		items.add("City 36");
		items.add("City 37");
		items.add("City 38");
		items.add("City 39");
		items.add("City 40");
		items.add("City 41");
		items.add("City 42");
		items.add("City 43");
		items.add("City 44");
		items.add("City 45");
		items.add("City 46");
		items.add("City 47");
		items.add("City 48");
		items.add("City 49");
		items.add("City 50");
		items.add("City 51");
		items.add("City 52");
		items.add("City 53");
		items.add("City 54");
		items.add("City 55");
		items.add("City 56");
		items.add("City 57");
		items.add("City 58");
		items.add("City 59");
		items.add("City 60");
		items.add("City 61");
		items.add("City 62");
		items.add("City 63");
		items.add("City 64");
		items.add("City 65");
		items.add("City 66");
		items.add("City 67");
		items.add("City 68");
		items.add("City 69");
		items.add("City 70");
		items.add("City 71");
		items.add("City 72");
		items.add("City 73");
		items.add("City 74");
		items.add("City 75");
		items.add("City 76");
		items.add("City 77");
		items.add("City 78");
		items.add("City 79");
		items.add("City 80");
		items.add("City 81");
		items.add("City 82");
		items.add("City 83");
		items.add("City 84");
		items.add("City 85");
		items.add("City 86");
		items.add("City 87");
		items.add("City 88");
		items.add("City 89");
		items.add("City 90");
		
		ArrayAdapter<String> sad = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, items);
		lv.setAdapter(sad);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int itemIndex, long id) {
				// TODO Auto-generated method stub
				String data = items.get(itemIndex);
				Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
				intent.putExtra("key", data);
				startActivity(intent);
			}			
		});
	}
}
