package com.example.fontexample;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	Button btn;
	TextView txt;
	EditText edit;
	
	Typeface tf;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		txt = (TextView) findViewById(R.id.textView1);
		edit = (EditText) findViewById(R.id.editText1);
		btn = (Button) findViewById(R.id.button1);
		
		String text = "বন্ধু তুমি আমার পাশে,আমার ভাল লাগা।";
		
		tf = Typeface.createFromAsset(this.getAssets(), "font/SolaimanLipi.ttf");
		txt.setTypeface(tf);
		edit.setTypeface(tf, Typeface.BOLD);
		btn.setTypeface(tf);
		
		txt.setText(text);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String text = edit.getText().toString();
				txt.setText(text);
			}
		});
	}
}
