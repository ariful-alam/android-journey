package com.example.labtest_2;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class Web_View extends Activity {
	WebView wb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web__view);
		
		String url = getIntent().getExtras().getString("url");
		
		wb = (WebView) findViewById(R.id.webView1);
		wb.getSettings().setJavaScriptEnabled(true);
		wb.getSettings().setBuiltInZoomControls(true);
		
		//wb.loadUrl("http://google.com"); //to load live page
		wb.loadUrl(url);
	}
}
