package com.example.sharedpreferenceandsqlite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class DashboardActivity extends Activity {
	Button logout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		
		String data[] = getIntent().getExtras().getStringArray("user");
        Toast.makeText(getApplicationContext(), "Welcome! "+data[0], Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Name: "+data[1]+"\n Phone: "+data[2], Toast.LENGTH_LONG).show();
        
        logout =(Button) findViewById(R.id.button1);
        logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}
}
