package com.syncmachine.intentexample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class SecondClass extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.second_activity);
        
        String value = getIntent().getExtras().getString("data");
        Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT).show();
	}
}
