package com.example.actionbarexample;

import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {//<-For Lower version Default: Activity
	
	Button hide, show, change;
	ActionBar actionbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		actionbar = getSupportActionBar();
		
		hide = (Button) findViewById(R.id.hide);
		hide.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionbar.hide();
			}
		});
		
		show = (Button) findViewById(R.id.show);
		show.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionbar.show();
			}
		});
		
		change = (Button) findViewById(R.id.change);
		change.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionbar.setIcon(R.drawable.user_add);
				actionbar.setTitle("Changed Action Bar");
				actionbar.setSubtitle("Subtitle");
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.call:
				Toast.makeText(getApplicationContext(), "Call item is clicked", Toast.LENGTH_SHORT).show();
				return true;
				
			case R.id.mail:
				Toast.makeText(getApplicationContext(), "Mail item is clicked", Toast.LENGTH_SHORT).show();
				return true;
				
			case R.id.send:
				Toast.makeText(getApplicationContext(), "Send item is clicked", Toast.LENGTH_SHORT).show();
				return true;
				
			case R.id.user:
				Toast.makeText(getApplicationContext(), "User item is clicked", Toast.LENGTH_SHORT).show();
				return true;
	
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
