package com.example.labtest_2;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Dashboard extends Activity {
	ListView lv;	
	ArrayList<String> items = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		
		lv = (ListView) findViewById(R.id.listView1);
		
		items.add("facebook");
		items.add("twitter");
		items.add("gmail");
		items.add("yahoo");
		
		ArrayAdapter<String> sad = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, items);
		lv.setAdapter(sad);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int itemIndex, long id) {
				// TODO Auto-generated method stub
				String data = items.get(itemIndex);
				Intent intent = new Intent(Dashboard.this, Web_View.class);
				intent.putExtra("url", "http://www."+data+".com");
				startActivity(intent);
			}			
		});
	}
}
