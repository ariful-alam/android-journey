package com.example.layoutexample;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ImageView;

public class FrameActivity extends Activity {
	ImageView bd1, bd2, bd3, bd4, bd5, bd6, bd7, bd8, bd9, bd10;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.framelayout);
		
		bd1 = (ImageView) findViewById(R.id.imageView1);
		bd2 = (ImageView) findViewById(R.id.imageView2);
		bd3 = (ImageView) findViewById(R.id.imageView3);
		bd4 = (ImageView) findViewById(R.id.imageView4);
		bd5 = (ImageView) findViewById(R.id.imageView5);
		bd6 = (ImageView) findViewById(R.id.imageView6);
		bd7 = (ImageView) findViewById(R.id.imageView7);
		bd8 = (ImageView) findViewById(R.id.imageView8);
		bd9 = (ImageView) findViewById(R.id.imageView9);
		bd10 = (ImageView) findViewById(R.id.imageView10);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.frame, menu);
		return true;
	}

}
