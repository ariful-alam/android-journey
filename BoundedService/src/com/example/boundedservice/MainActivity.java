package com.example.boundedservice;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.boundedservice._Service.LocalBinder;

public class MainActivity extends Activity {
	Button btn1, btn2, btn3;
	TextView _time;
	
	public _Service service;
	public static boolean isBound = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		btn1 = (Button) findViewById(R.id.button1);//GET TIME
		btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(isBound){
					String time = service.getTime();
					_time.setText(time);
				}else{
					_time.setText("Service is not bounded");
				}
			}
		});
		btn2 = (Button) findViewById(R.id.button2);//BIND
		btn2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, _Service.class);
				bindService(intent, conn, Context.BIND_AUTO_CREATE);
			}
		});
		btn3 = (Button) findViewById(R.id.button3);//UNBIND
		btn3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				unbindService(conn);
			}
		});
		
		_time = (TextView) findViewById(R.id.textView1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	protected ServiceConnection conn = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			// TODO Auto-generated method stub
			service = null;
			isBound = false;
			_time.setText("Application Crashed. Service is not bounded");
		}
		
		@Override
		public void onServiceConnected(ComponentName arg0, IBinder servicebinder) {
			// TODO Auto-generated method stub
			LocalBinder binder = (LocalBinder)servicebinder;
			
			service = binder.getService();
			isBound = true;
			_time.setText(service.getTime());
		}
	};

}
