package com.example.sensorchecker;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	Button check;
	TextView message;
	
	
	SensorManager sm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		check = (Button) findViewById(R.id.button1);
		check.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
				List<android.hardware.Sensor> sensorList = sm.getSensorList(Sensor.TYPE_ALL);
				
				if(sensorList != null){
					String text = "";
					for(int i = 0; i<sensorList.size(); i++){
						String sensorName = sensorList.get(i).getName();
						text += sensorName + "\n";
					}
					message.setText(text);
				}
			}
		});
		
		message = (TextView) findViewById(R.id.textView1);
	}
}
