package com.example.listviewexample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayActivity extends Activity {
	TextView tv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);
		
		tv = (TextView) findViewById(R.id.textView1);
		
		String data = getIntent().getExtras().getString("key");
		tv.setText(data);
	}
}
