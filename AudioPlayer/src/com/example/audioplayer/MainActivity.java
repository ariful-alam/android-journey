package com.example.audioplayer;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button start, pause;
	MediaPlayer mp;
	boolean mp_state = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		start = (Button) findViewById(R.id.start);
		pause = (Button) findViewById(R.id.pause);
		
		mp = MediaPlayer.create(this, R.raw.audio);
		mp.setLooping(true);
		
		start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(!mp_state){
					mp.start();
					mp_state = true;
				}else{
					Toast.makeText(getApplicationContext(), "Already started", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		pause.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(mp_state){
					mp.pause();
					mp_state = false;
				}else{
					Toast.makeText(getApplicationContext(), "Already paused", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}
