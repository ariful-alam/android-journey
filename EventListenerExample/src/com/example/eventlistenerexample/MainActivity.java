package com.example.eventlistenerexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity /*implements OnClickListener*/ {
	Button btn1, btn2;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        btn1 = (Button)findViewById(R.id.button1);
        btn2 = (Button)findViewById(R.id.button2);
        //btn1.setOnClickListener(this);
        //btn2.setOnClickListener(this);
        
        /*btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Ouch! You clicked Me ;)", Toast.LENGTH_SHORT).show();
				
			}
		});*/
    }

	/*@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view.getId() == R.id.button1){
			Toast.makeText(getApplicationContext(), "Ouch! Button 1 clicked ;)", Toast.LENGTH_SHORT).show();
		}else if(view.getId() == R.id.button2){
			Toast.makeText(getApplicationContext(), "Ouch! Button 2 clicked ;)", Toast.LENGTH_SHORT).show();
		}
		return;
	}*/
	
	public void methodForButton(View view){
		//Toast.makeText(getApplicationContext(), "Ouch! Button 1 clicked using method ;)", Toast.LENGTH_SHORT).show();
		if(view.getId() == R.id.button1){
			Toast.makeText(getApplicationContext(), "Ouch! Button 1 clicked using function ;)", Toast.LENGTH_SHORT).show();
		}else if(view.getId() == R.id.button2){
			Toast.makeText(getApplicationContext(), "Ouch! Button 2 clicked using function ;)", Toast.LENGTH_SHORT).show();
		}
		return;
	}

	/*@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}*/
}
