package com.example.sensoraccelarate;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sensoraccessoledator.R;

public class MainActivity extends Activity implements SensorEventListener {
	TextView tx, ty, tz;
	SensorManager manager;
	Sensor accelarate;
	long lastTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tx = (TextView) findViewById(R.id.textView1);
		ty = (TextView) findViewById(R.id.textView2);
		tz = (TextView) findViewById(R.id.textView3);
		
		manager = (SensorManager) getSystemService(SENSOR_SERVICE);
		accelarate = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		if(accelarate == null){
			Toast.makeText(getApplicationContext(), "Accelerometer sensor is not available", Toast.LENGTH_SHORT).show();
			finish();
		}else{
			lastTime = System.currentTimeMillis();
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		manager.unregisterListener(this);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		manager.registerListener(this, accelarate, SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
			long currentTime = System.currentTimeMillis();
			if((currentTime - lastTime) > 1000){
				float AxsisX = event.values[0];
				float AxsisY = event.values[1];
				float AxsisZ = event.values[2];
				
				tx.setText("X: "+AxsisX);
				ty.setText("Y: "+AxsisY);
				tz.setText("Z: "+AxsisZ);
				
				lastTime = currentTime;				
			}
		}		
	}

}
