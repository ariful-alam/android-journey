package com.ict.actionbarsupported;

import android.annotation.SuppressLint;
import android.support.v7.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	
	Button btnShow,btnHide,btnChange;
	
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnHide=(Button)findViewById(R.id.btnHide);
		btnShow=(Button)findViewById(R.id.btnShow);
		btnChange=(Button)findViewById(R.id.btnChange);
		
		final ViewGroup v = (ViewGroup)LayoutInflater.from(this).inflate(R.layout.action_bar_view, null);		
		actionBar=getSupportActionBar();
		
		btnHide.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				actionBar.hide();
			}
		});
		
		btnShow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				actionBar.show();
			}
		});
		
		
		btnChange.setOnClickListener(new OnClickListener() {
			
			@SuppressLint("NewApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub				
				actionBar.setIcon(R.drawable.user_add);
				actionBar.setTitle("Changed Action Bar");
				actionBar.setSubtitle("Testing");
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.call:
			Toast.makeText(getApplicationContext(), "Call is clicked", Toast.LENGTH_LONG).show();
			return true;			
		case R.id.mail:
			Toast.makeText(getApplicationContext(), "Mail is clicked", Toast.LENGTH_LONG).show();
			return true;
		case R.id.send:
			Toast.makeText(getApplicationContext(), "Send is clicked", Toast.LENGTH_LONG).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}  
	
}
