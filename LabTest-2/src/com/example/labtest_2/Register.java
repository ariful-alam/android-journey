package com.example.labtest_2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends Activity {
	EditText email;
	EditText password;
	Button register;
	
	SharedPreferences sp;
	SharedPreferences.Editor spe;
	
	SQLiteDatabase db;
	static String database = "sync_account";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		sp = getSharedPreferences("database", MODE_PRIVATE);
		spe = sp.edit();
		
		int isDBexists = sp.getInt("db", 0);
		if(isDBexists == 0){
			createDB();
			spe.putInt("db", 1);
			spe.commit();
		}
		
		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.password);
		
		register = (Button) findViewById(R.id.register);
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				db = openOrCreateDatabase(database, 0, null);
				db.execSQL("INSERT INTO users (username, password) VALUES ('"+email.getText().toString()+"', '"+password.getText().toString()+"')");
				db.close();
				
				Toast.makeText(getApplicationContext(), "Account craeted for "+email.getText()+" successful! :)", Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(Register.this, Login.class);
				startActivity(intent);
			}
		});
	}
	
	public void createDB(){
		db = openOrCreateDatabase(database, 0, null);
		String create = "CREATE TABLE IF NOT EXISTS users(id INTEGER AUTO_INCREMENT PRIMARY KEY, username VARCHAR UNIQUE, password VARCHAR);";
		db.execSQL(create);
		db.close();
		//Toast.makeText(getApplicationContext(), "Table created", Toast.LENGTH_SHORT).show();
	}
}
