package com.example.webviewexample;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebView;

public class MainActivity extends Activity {
	WebView wb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		wb = (WebView) findViewById(R.id.webView1);
		wb.getSettings().setJavaScriptEnabled(true);
		wb.getSettings().setBuiltInZoomControls(true);
		
		//wb.loadUrl("http://google.com"); //to load live page
		wb.loadUrl("file:///android_asset/index.html");
	}
}
