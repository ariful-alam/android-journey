package com.example.labtest_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	Button sports, social, news;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		sports = (Button) findViewById(R.id.sports);
		social = (Button) findViewById(R.id.social);
		news = (Button) findViewById(R.id.news);
		
		sports.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, Sports.class);
				startActivity(intent);				
			}
		});
		
		social.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, Social.class);
				startActivity(intent);				
			}
		});
	}
}
