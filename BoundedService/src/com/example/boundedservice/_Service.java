package com.example.boundedservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

public class _Service extends Service {
	private final IBinder localbinder = new LocalBinder();
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "Service Binded", Toast.LENGTH_SHORT).show();
		return localbinder;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "Service Created", Toast.LENGTH_SHORT).show();
		//super.onCreate();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		MainActivity.isBound = false;
		Toast.makeText(getApplicationContext(), "Service Destroied", Toast.LENGTH_SHORT).show();
		//super.onDestroy();
	}
	
	public String getTime(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
		String time = df.format(calendar.getTime());
		return time;
	}
	
	public class LocalBinder extends Binder{
		_Service getService(){
			return _Service.this;
		}
	}
}
