package com.syncmachine.syncmachine;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Toast.makeText(getApplicationContext(), "onCreate() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	Toast.makeText(getApplicationContext(), "onStart() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    protected void onRestart() {
    	// TODO Auto-generated method stub
    	super.onRestart();
    	Toast.makeText(getApplicationContext(), "onRestart() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	Toast.makeText(getApplicationContext(), "onResume() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	Toast.makeText(getApplicationContext(), "onPause() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    protected void onStop() {
    	// TODO Auto-generated method stub
    	super.onStop();
    	Toast.makeText(getApplicationContext(), "onStop() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	Toast.makeText(getApplicationContext(), "onDestroy() called", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
