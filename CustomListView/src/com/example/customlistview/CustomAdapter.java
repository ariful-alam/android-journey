package com.example.customlistview;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter {
	LayoutInflater inFlater;
	Context context;
	List<String> countrylist;
	
	public CustomAdapter(Context context, List<String> countrylist){
		this.context = context;
		this.countrylist = countrylist;
		
		inFlater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return countrylist.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return countrylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Toast.makeText(context, "getView " + position + " " + convertView, Toast.LENGTH_SHORT).show();
		
		ViewHolder holder = null;
		
		if(convertView == null){
			convertView = inFlater.inflate(R.layout.item, null);
			
			holder = new ViewHolder();
			holder.txtView = (TextView) convertView.findViewById(R.id.name);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtView.setText(countrylist.get(position));
		return convertView;
	}
	
	public static class ViewHolder{
		public TextView txtView;
	}
}
