package com.example.videoplayer;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends Activity {
	VideoView vv;
	MediaController mc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		vv = (VideoView) findViewById(R.id.videoView1);
		mc = new MediaController(this);
		
		vv.setMediaController(mc);
		//Following code to run video from resource folder
		vv.setVideoURI(Uri.parse("android.resource://"+getPackageName().toString()+"/raw/samplevideo"));
		
		//Following code to run video from external path
		vv.setVideoPath(Environment.getExternalStorageDirectory()+"/path_to_directory/file.ext");
		
		vv.start();		
	}
}
