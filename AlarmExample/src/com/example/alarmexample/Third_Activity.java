package com.example.alarmexample;

import android.app.Activity;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Third_Activity extends Activity {
	Button stop;
	Uri uri;
	Ringtone ring;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_third_);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		ring = RingtoneManager.getRingtone(this, uri);
		
		Vibrator vibrate = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrate.vibrate(3000);
		
		ring.play();
		
		stop = (Button) findViewById(R.id.button1);
		stop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ring.stop();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.third_, menu);
		return true;
	}

}
