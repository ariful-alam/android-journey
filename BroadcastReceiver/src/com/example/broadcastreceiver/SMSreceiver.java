package com.example.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

public class SMSreceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
			Bundle bundle = intent.getExtras();
			if(bundle != null){
				SmsMessage[] messages = null;
				String number = null;
				String message = "";
				Object[] object = (Object[]) bundle.get("pdus");
				
				messages = new SmsMessage[object.length];
				for(int i = 0; i < object.length; i++){
					messages[i] = SmsMessage.createFromPdu((byte[]) object[i]);
					if(number == null){
						number = messages[i].getOriginatingAddress();
					}
					message += messages[i].getMessageBody();
				}
				SmsManager manager = SmsManager.getDefault();
				manager.sendTextMessage(number, null, message, null, null);
				
				Intent intnt = new Intent(context, MainActivity.class);
				intnt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intnt);
			}
		}
	}

}
