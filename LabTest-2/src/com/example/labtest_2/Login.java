package com.example.labtest_2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {
	EditText email;
	EditText password;
	Button login;
	CheckBox checkbox;
	
	SharedPreferences sp;
	SharedPreferences.Editor spe;
	
	SQLiteDatabase db;
	
	static String database = "sync_account";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		sp = getSharedPreferences("database", MODE_PRIVATE);
		spe = sp.edit();
		
		int isDBexists = sp.getInt("db", 0);
		if(isDBexists == 0){
			createDB();
			spe.putInt("db", 1);
			spe.commit();
		}
		
		String getEmail = sp.getString("email", "0");
		String getPass = sp.getString("password", "0");
		if(!getEmail.equals("0")){
			email.setText(getEmail);
			password.setText(getPass);
		}
		
		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.password);
		checkbox = (CheckBox) findViewById(R.id.checkbox);
		
		login = (Button) findViewById(R.id.log_in);
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				db = openOrCreateDatabase(database, 0, null);
				Cursor cursor = db.rawQuery("SELECT * FROM users WHERE username = '"+email.getText().toString()+"' AND password = '"+password.getText().toString()+"';", null);
				
				if(cursor.getCount() < 1){
					Toast.makeText(getApplicationContext(), "Login failed. :( Try again", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getApplicationContext(), "Login Successful. :)", Toast.LENGTH_SHORT).show();
					
					if(checkbox.isChecked()){
						spe.putString("email", email.getText().toString());
						spe.putString("password", password.getText().toString());
						spe.commit();
					}else{
						spe.putString("email", "");
						spe.putString("password", "");
						spe.commit();
					}
					
					Intent intent = new Intent(Login.this, Dashboard.class);
					startActivity(intent);
				}
				db.close();
			}
		});
	}

	public void createDB(){
		db = openOrCreateDatabase(database, 0, null);
		String create = "CREATE TABLE IF NOT EXISTS users(id INTEGER AUTO_INCREMENT PRIMARY KEY, username VARCHAR UNIQUE, password VARCHAR);";
		db.execSQL(create);
		db.close();
		//Toast.makeText(getApplicationContext(), "Table created", Toast.LENGTH_SHORT).show();
	}
}
