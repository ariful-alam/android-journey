package com.example.frameanimation;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.view.Menu;
import android.widget.ImageView;

public class MainActivity extends Activity {
	ImageView iv;
	AnimationDrawable ad;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		iv = (ImageView) findViewById(R.id.imageView1);
		iv.setImageResource(R.drawable.frameanimation);
		
		ad = (AnimationDrawable) iv.getDrawable();
		ad.start();
	}
}
