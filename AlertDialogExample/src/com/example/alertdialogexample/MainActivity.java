package com.example.alertdialogexample;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button btn1, btn2, btn3, btn4;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		btn1 = (Button) findViewById(R.id.btn1);//simple
		btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder alertbuilder;
				alertbuilder = new AlertDialog.Builder(MainActivity.this);
				alertbuilder.setTitle("Simple Dialog");
				alertbuilder.setIcon(R.drawable.ic_launcher);
				alertbuilder.setMessage("This is a simple dialog. Click any button");
				alertbuilder.setCancelable(false);
				
				alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Toast.makeText(getApplicationContext(), "OK is clicked.", Toast.LENGTH_SHORT).show();
					}
				});
				
				alertbuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Toast.makeText(getApplicationContext(), "CANCEL is clicked.", Toast.LENGTH_SHORT).show();
					}
				});
				
				alertbuilder.setNeutralButton("NEUTRAL", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Toast.makeText(getApplicationContext(), "NEUTRAL is clicked.", Toast.LENGTH_SHORT).show();
					}
				});
				
				AlertDialog alert = alertbuilder.create();
				alert.show();
			}
		});
		
		btn2 = (Button) findViewById(R.id.btn2);//single
		btn2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder alertbuilder;
				alertbuilder = new AlertDialog.Builder(MainActivity.this);
				alertbuilder.setTitle("Item Dialog");
				alertbuilder.setIcon(R.drawable.ic_launcher);
				alertbuilder.setCancelable(false);
				
				final String[] items = {"RED", "GREEN", "BLUE", "YELLOW", "BLACK", "WHITE"};
				alertbuilder.setItems(items, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int pos) {
						String item = items[pos];
						Toast.makeText(getApplicationContext(), item + " is clicked.", Toast.LENGTH_SHORT).show();
					}
				});
				
				alertbuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Toast.makeText(getApplicationContext(), "Dialog is clicked.", Toast.LENGTH_SHORT).show();
					}
				});
				
				AlertDialog alert = alertbuilder.create();
				alert.show();
			}
		});
		
		btn3 = (Button) findViewById(R.id.btn3);//multi
		btn3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder alertbuilder;
				alertbuilder = new AlertDialog.Builder(MainActivity.this);
				alertbuilder.setTitle("MultiChoice Item Dialog");
				alertbuilder.setIcon(R.drawable.ic_launcher);
				alertbuilder.setCancelable(false);
				
				final String[] items = {"RED", "GREEN", "BLUE", "YELLOW", "BLACK", "WHITE"};
				
				boolean[] status = {false, true, true, false, true, false};
				
				final ArrayList<String> selected = new ArrayList<String>();
				selected.add(items[1]);
				selected.add(items[2]);
				selected.add(items[4]);
				
				alertbuilder.setMultiChoiceItems(items, status, new DialogInterface.OnMultiChoiceClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int pos, boolean isChecked) {
						if(isChecked){
							selected.add(items[pos]);
						}else{
							selected.remove(items[pos]);
						}
					}
				});
				
				alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Toast.makeText(getApplicationContext(), "OK is clicked.", Toast.LENGTH_SHORT).show();
						String text = "Selected Items:";
						for(int i = 0; i < selected.size(); i++){
							text += " " + selected.get(i);
						}
						Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
					}
				});
				
				AlertDialog alert = alertbuilder.create();
				alert.show();
			}
		});
		
		btn4 = (Button) findViewById(R.id.btn4);//custom
		btn4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
				View view = inflater.inflate(R.layout.customlayout, null);
				
				Button btnDialog = (Button) view.findViewById(R.id.button1);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setView(view);
				builder.setCancelable(false);
				final AlertDialog dialog = builder.create();
				
				btnDialog.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						Toast.makeText(getApplicationContext(), "Custom Dialog button is clicked", Toast.LENGTH_SHORT).show();
						dialog.cancel();
					}
				});
				
				dialog.show();
			}
		});
	}
}