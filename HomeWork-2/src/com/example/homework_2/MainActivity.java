package com.example.homework_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    Button btn;
    TextView txt;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        btn = (Button) findViewById(R.id.submit);
        txt = (TextView) findViewById(R.id.txtBox);
        
        btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Toast.makeText(getApplicationContext(), txt.getText().toString(), Toast.LENGTH_LONG).show();
				Intent intent = new Intent(MainActivity.this, Page2.class);
				intent.putExtra("data", txt.getText().toString());
				startActivity(intent);				
			}
		});
    }    
}
