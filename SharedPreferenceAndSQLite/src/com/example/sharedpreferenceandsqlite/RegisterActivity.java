package com.example.sharedpreferenceandsqlite;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {
	EditText name, username, password, phone;
	Button register;
	
	SQLiteDatabase db;
	static String database = "sync_account";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		name 		= (EditText) findViewById(R.id.name);
		username 	= (EditText) findViewById(R.id.username);
		password 	= (EditText) findViewById(R.id.password);
		phone 		= (EditText) findViewById(R.id.phone);
		
		register	= (Button) findViewById(R.id.register);
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				db = openOrCreateDatabase(database, 0, null);
				db.execSQL("INSERT INTO users (username, password, name, phone) VALUES ('"+username.getText()+"', '"+password.getText()+"', '"+name.getText()+"', '"+phone.getText()+"')");
				db.close();
				
				Toast.makeText(getApplicationContext(), "User "+username.getText()+" created successfully!", Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}
}
